#include <stdio.h>
#include <unistd.h>
#include <signal.h>

void jelkezelo(int signal)
         {
        printf("Szeretnéd megszakitani? Nyomj Ctrl+C-t");
        }

int main(void)
{

    for(;;)
    {
        if(signal(SIGINT, jelkezelo)==SIG_IGN){
            signal(SIGINT, SIG_IGN);
	}
    }


}

