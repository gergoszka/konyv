%{
#include <stdio.h>
int szamlalo = 0;
%}
digit	[0-9]

%%
{digit}*(\.{digit}+)?	{++szamlalo; 
    printf("[Valos szam= %s %f]", yytext, atof(yytext));}

%%
int main ()
{
	yylex ();
	printf("%d db valós számot adtál meg\n", szamlalo);

 return 0;
}
