#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

int* mut(int a)		//egészre mutató mutatót visszaadó függvény
{
	int* am = &a;
	return am;
}

int* (*mut_ptr)(int) = &mut; 			//egészre mutató mutatót visszaadó függvényre mutató mutató


int egesz(int x, int y)
{
	return y;
}

int (*egesz_ptr)(int, int) = &egesz;

int** egesz_vissza(int x)
{
	return egesz_ptr; 
}

int main()
{
	int a = 45;		//egész
	int* am = &a;	//egészre mutató mutató
	int t [5];		//egészek tömbje
	int (&tr)[5]=t;		//egészek tömbjének referenciája
	int* tm [6];		//egészre mutató mutatók tömbje
	

	cout<< mut(a) << endl;
	cout<< (*mut_ptr)(a) << endl;
	cout<< egesz_vissza(a) << endl;

}
