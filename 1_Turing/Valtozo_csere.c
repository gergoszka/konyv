#include <stdio.h>
int main() 
{ 
    int a=42, b=666;

    printf("elso=%d masodik=%d\n",a,b);

	int c=a;	//Csere segédváltozóval
	a=b;   
	b=c;
/*
	a = a - b;	//Csere összeadás-kivonással
	b = a + b;
	a = b - a;


	a = a ^ b;	//Csere EXOR-al
	b = a ^ b;
	a = a ^ b;	
*/
    printf("Csere utan: \n");
    printf("elso=%d masodik=%d\n",a,b);
    
    return 0; 
}
