#ifndef ANT_H
#define ANT_H

class Ant
{

public:
    int x;
    int y;
    int dir;

    Ant(int x, int y): x(x), y(y) {
        
        dir = qrand() % 8;
        
    }

};

typedef std::vector<Ant> Ants;

#endif
