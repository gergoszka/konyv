;iteratív faktoriális
(defun fakt_i (n)(loop for i from 1 to n for result = 1 then (* result i) finally return result)

;rekurzív faktoriális
(defun fakt_r (n) (if(< n 1)1(* n (fakt_r(- n 1)))))   
				